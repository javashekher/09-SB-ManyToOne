package com.ashokit.jpa.runner;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.stereotype.Component;

import com.ashokit.jpa.entities.ProductEntity;
import com.ashokit.jpa.repository.ProductEntityRepository;

@Component
public class MyRunner implements CommandLineRunner {
	@Autowired
	ProductEntityRepository productRepo;

	@Override
	public void run(String... args) throws Exception {
		/*
		VendorEntity vEntity = new VendorEntity();
		vEntity.setVendorId(102);  vEntity.setVendorName("Oracle");
		
		ProductEntity pEntity1 = new ProductEntity();
		pEntity1.setProductId(20001); pEntity1.setProductName("Weblogic");
		ProductEntity pEntity2 = new ProductEntity();
		pEntity2.setProductId(20002); pEntity2.setProductName("JDeveloper");
		
		pEntity1.setVendorEntity(vEntity);
		pEntity2.setVendorEntity(vEntity);
		
		productRepo.save(pEntity1);
		productRepo.save(pEntity2);
		*/
		
		/*
		Optional<ProductEntity> opt = productRepo.findById(20001);
		if(opt.isPresent()) {
			ProductEntity pE = opt.get();
			System.out.println(pE.getProductId() + ", " + pE.getProductName());
		}
		*/
		
		productRepo.deleteById(20002);
		
		

	}

}
