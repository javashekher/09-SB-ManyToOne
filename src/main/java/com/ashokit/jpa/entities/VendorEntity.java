package com.ashokit.jpa.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;

@Entity
@Table( name = "VENDOR" )
@Data
public class VendorEntity {
	@Id
	@Column( name = "VEN_ID")
	private Integer vendorId;
	
	@Column( name = "VEN_NAME" , length = 20 )
	private String vendorName;

}
